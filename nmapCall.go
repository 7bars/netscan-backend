package main

import (
	"bytes"
	"log"
	"os/exec"
	"strings"
)

func pingNmap(addrv4 string) {
	var _addrv4 []string
	_addrv4 = append(_addrv4, addrv4)
	_addrv4 = append(_addrv4, "-sP")
	_addrv4 = append(_addrv4, "-oX")
	_addrv4 = append(_addrv4, "./ping.xml")
	// addrv4 = append(addrv4, "-sP")
	cmd := exec.Command("nmap", _addrv4...)
	// cmd := exec.Command("nmap", "192.168.1.68", "-sP", "-oX", "./ping.xml")
	cmd.Stdin = strings.NewReader("some input")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func scanNmap(addrv4 string, name string) {
	var _addrv4 []string
	_addrv4 = append(_addrv4, addrv4)
	_addrv4 = append(_addrv4, "-sV")
	_addrv4 = append(_addrv4, "-O")
	_addrv4 = append(_addrv4, "-oX")
	_addrv4 = append(_addrv4, "./"+name+".xml")
	// addrv4 = append(addrv4, "-sP")
	cmd := exec.Command("nmap", _addrv4...)
	// cmd := exec.Command("nmap", "192.168.1.68", "-sP", "-oX", "./ping.xml")
	cmd.Stdin = strings.NewReader("some input")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
