package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

//listIP for json
type listIP struct {
	IP []string
}

//IP for json
type IP struct {
	IP string
}

func responseScan(w http.ResponseWriter, r *http.Request) {
	var lstHost listIP
	var allIP = ""
	time := time.Now().Format("20060102150405")
	err := json.NewDecoder(r.Body).Decode(&lstHost)
	if err != nil {
		fmt.Println(err)
	}
	for i := range lstHost.IP {
		if len(lstHost.IP)-1 != i {
			allIP += lstHost.IP[i] + " "
		} else {
			allIP += lstHost.IP[i]
		}
	}

	scanNmap(allIP, time)
	data, err := ioutil.ReadFile(time + ".xml")
	if err != nil {
		fmt.Println(err)
	}

	result, err := Parse(data)
	if err != nil {
		fmt.Println(err)
	}
	b, err := json.Marshal(result)
	if err != nil {
		fmt.Print("Misstake is created JSON answer")
	}
	w.Write(b)

}

func responseIP(w http.ResponseWriter, r *http.Request) {
	var lstIP listIP
	var err error
	lstIP.IP, err = externalIP()
	if err != nil {
		fmt.Println(err)
	}

	b, err := json.Marshal(lstIP)
	if err != nil {
		fmt.Print("Misstake is created JSON answer")
	}
	w.Write(b)
}

func availableHost(w http.ResponseWriter, r *http.Request) {
	var ip IP
	var lstHost listIP
	err := json.NewDecoder(r.Body).Decode(&ip)
	if err != nil {
		fmt.Println(err)
	}
	pingNmap(ip.IP)

	data, err := ioutil.ReadFile("ping.xml")
	if err != nil {
		fmt.Println(err)
	}

	res, err := Parse(data)
	if err != nil {
		fmt.Println(err)
	}
	lstHost.IP = make([]string, len(res.Hosts))
	for i := range res.Hosts {
		for j := range res.Hosts[i].Addresses {
			if res.Hosts[i].Addresses[j].AddrType == "ipv4" {
				lstHost.IP[i] = res.Hosts[i].Addresses[j].Addr
			}
		}
	}

	if len(lstHost.IP) == 0 {
		lstHost.IP = append(lstHost.IP, "0.0.0.0")
	}

	b, err := json.Marshal(lstHost)
	if err != nil {
		fmt.Print("Misstake is created JSON answer")
	}
	w.Write(b)

}

func main() {
	http.HandleFunc("/availableHost", availableHost)
	http.HandleFunc("/getIP", responseIP)
	http.HandleFunc("/scan", responseScan)

	http.ListenAndServe(":8181", nil)
}
