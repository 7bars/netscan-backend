package main

import (
	"errors"
	"fmt"
	"net"
	"regexp"
)

func externalIP() ([]string, error) {
	var ip4 []string
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			ip4 = append(ip4, parseIP(ip.String()))
		}
	}
	if len(ip4) != 0 {
		return ip4, nil
	}
	return nil, errors.New("are you connected to the network?")
}

func parseIP(checkIPBody string) string {
	var str = ""
	reg, err := regexp.Compile("[0-9]+\\.")
	if err != nil {
		fmt.Println(err)
	}

	strArr := reg.FindAllString(checkIPBody, -1)
	for i := range strArr {
		str += strArr[i]
	}
	str += "1-255"

	return str
}
